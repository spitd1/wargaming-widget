# Demo
http://max.crazycarrot.net/wargaming-widget/

# Wargaming Widget

Wargaming Front-End Developer Test using HTML, CSS/Sass, React.

**1. Search Fields** - The button and the field must occupy all free space of the block. The width of the button depends on the text it contains; the search field takes up all the remaining free space of the block.

**2. Adaptive List (optional)** - There is a list of tags. With a resolution of 1300px, several tags are displayed in a row on the screen. With a resolution of 800px, this list should be converted to a drop-down.

**3. Element Widget** - Write a "widget" to select 3 elements from a certain list (initially set as an array).
- A list of already selected items is displayed (no more than three).
- By clicking on the "Change my choice" button, a dialog opens with a list of all elements (a scrollable list of fixed height), as well as a search field and filter.
- Checkboxes of already selected items are checked, and the selected items are duplicated as blocks at the bottom of the dialog box.
- Search: as you type characters, the list of elements is filtered (using substring search).
- Additional filter (selectbox) - filter by element number (> 10,> 50,> 100).
- Search and filter complete each other.
- You can select no more than three items, in case three items are selected, the remaining checkboxes become disabled.
- The selected items have a link "X" which removes the item from the list of selected items.
- Clicking on the "Save" button closes the dialog box and the list of selected items on the main page is updated from the dialog.
- When you click on the "Cancel" button, the dialog box closes and the list of selected items on the main page remains unchanged.

# Development
1. clone project and install with NPM or Yarn 
2. run `npm start` for development mode
3. run `npm run build` for production, project files will be exported in _dist/_ folder

# Changelog
| Version | Changes |
| ------ | ------ |
| **1.0.0** | - added components Search Fields, Adaptive List and Element Widget |
