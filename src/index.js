import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import '@babel/polyfill';

import 'sanitize.css/sanitize.css';
import 'sanitize.css/forms.css';
import 'sanitize.css/typography.css';
import './styles/style.scss';

ReactDOM.render(<App />, document.getElementById('app'));
