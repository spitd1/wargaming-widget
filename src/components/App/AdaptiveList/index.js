import React from 'react';

const AdaptiveList = () => {
  return (
    <section className='section section--adaptivelist'>
      <header><h2 className='section__title'>2. Adaptive List</h2></header>
      <div className='adaptivelist__toggle'>&#9776; &nbsp;World of Warplanes</div>
      <ul className='adaptivelist'>
        <li className='adaptivelist__item'><a href='#'>World of Tanks</a></li>
        <li className='adaptivelist__item adaptivelist__item--selected'><a href='#'>World of Warplanes</a></li>
        <li className='adaptivelist__item'><a href='#'>World of Warships</a></li>
      </ul>
    </section>
  );
};

export default AdaptiveList;
