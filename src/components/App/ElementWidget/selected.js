import React from 'react';
import { PropTypes } from 'prop-types';
import Button from '../Form/Element/button';


const ElementWidgetSelected = ({ selectedElements, handleRemoveElement }) => {
  return (
    <ul className='elementwidget__badges'>
      {selectedElements.map(element => {
        return (
          <li key={element.guid}>
            <div className='elementwidget__badge'>
              <span className='elementwidget__badgeLabel'>{element.name}</span>
              <Button
                type='button'
                value='Remove'
                buttonClassNames='btn btn--close elementwidget__removeBadge'
                sronly={true}
                onClick={() => handleRemoveElement(element)}
              />
            </div>
          </li>
        )
      })}
    </ul>
  );
};

ElementWidgetSelected.defaultProps = {
  onClick: () => {}
};

ElementWidgetSelected.propTypes = {
  selectedElements: PropTypes.array.isRequired
};

export default ElementWidgetSelected;