import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import { remove } from 'lodash-es';
import Button from '../Form/Element/button';
import ElementWidgetSelected from './selected';
import ElementWidgetList from './list';
import JSONdata from '../../../data/json/elements.json';

const elementList = JSONdata.elements;


class ElementWidget extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedElements: [],
      listOpen: false
    };
  }

  handleSaveWidgetElements = elements => {
    console.log('save');
    this.setState({
      selectedElements: elements
    })
  }

  handleCloseList = () => {
    console.log('close');
    this.setState({
      listOpen: false
    })
  }

  handleRemoveElement = element => {
    const { selectedElements } = this.state;
    console.log('remove');
    console.log(remove(selectedElements, element))


    this.setState({
      selectedElements: [...selectedElements]
    });
  }

  render() {
    const { selectedElements, listOpen } = this.state;

    return (
      <section className='section section--elementwidget'>
        <header><h2 className='section__title'>3. Element widget</h2></header>
        <div className='elementwidget__box'>
          <div className='elementwidget__selected'>
            <div className=''><h3 className='elementwidget__title'>Select items</h3></div>
            <div className='elementwidget__selectedCounter'>{selectedElements.length > 0 ? `You have currently ${selectedElements.length} selected items.` : 'You have currently selected no item.'}</div>
            <ElementWidgetSelected
              selectedElements={selectedElements}
              handleRemoveElement={this.handleRemoveElement}
            />
            <Button
              type='button'
              value='change my choice'
              buttonClassNames='btn btn--secondary'
              onClick={() => {this.setState({listOpen: true, selectedElements: [...selectedElements]})}}
            />
          </div>
          <ElementWidgetList
            elementList = {elementList}
            selectedElements={selectedElements}
            handleChange={this.handleElement}
            handleSaveWidgetElements={this.handleSaveWidgetElements}
            listOpen={listOpen}
            handleCloseList={this.handleCloseList}
          />
        </div>
      </section>
    )
  }
}

export default ElementWidget;
