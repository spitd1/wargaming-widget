import React, { PureComponent, Fragment } from 'react';
import Button from '../Form/Element/button';
import Input from '../Form/Element/input';

class ElementWidgetFilter extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { filter, searchTerm, handleFilter, handleSearch, handleCloseList } = this.props;
    return (
      <Fragment>
        <div className='elementwidget__filter'>
          <div className='elementwidget__filterHeader'>
            <div className='elementwidget__filterTitle'>Select items</div>
            <div className='elementwidget__filterClose'>
              <Button
                type='button'
                value='Close'
                buttonClassNames='btn btn--close'
                sronly={true}
                onClick={() => handleCloseList()}
              />
            </div>
          </div>
          <div className='elementwidget__filterNav'>
            <div className='elementwidget__filterSearch'>
              <label>Search</label>
              <Input
                type='text'
                name='search'
                value={searchTerm}
                handleChange={handleSearch}
                maxLength={50}
              />

            </div>
            <div className='elementwidget__filterNum'>
              <label>Filter</label>
              <select
                className=''
                name=''
                value={filter}
                onChange={handleFilter}
              >
                <option value='0'>&gt;all</option>
                <option value='10'>&gt;10</option>
                <option value='50'>&gt;50</option>
                <option value='100'>&gt;100</option>
              </select>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default ElementWidgetFilter;
