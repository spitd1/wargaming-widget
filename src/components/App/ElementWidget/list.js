import React, { PureComponent } from 'react';
import { remove, some, find } from 'lodash-es';
import Button from '../Form/Element/button';
import Checkbox from '../Form/Element/checkbox';
import ElementWidgetFilter from './filter';
import ElementWidgetSelected from './selected';

class ElementWidgetList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      elements: [],
      selectedElements: [],
      searchTerm: '',
      filter: 0
    };
    this.handleElement = this.handleElement.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }

  componentDidMount() {
    const { selectedElements } = this.props;

    this.setState({selectedElements: [...selectedElements]});
  }

  handleElement(e) {
    let { value } = e.target;
    const { name, checked } = e.target;
    const { selectedElements } = this.state;

    const element = JSON.parse(value);
    
    if (find(selectedElements, element)) {
      remove(selectedElements, element);
      value = [
        ...selectedElements
      ];
    } else {
      value = [
        ...selectedElements,
        element
      ];
    }

    this.setState({
      selectedElements: value
    });
  }

  handleSearch(e) {
    let { value } = e.target;
    this.setState({
      searchTerm: value
    })
  }

  handleFilter(e) {
    let { value } = e.target;
    this.setState({
      filter: value
    })
  }

  handleResetList = () => {
    this.setState({
      selectedElements: [],
      searchTerm: '',
      filter: 0
    })
  }

  handleRemoveElement = element => {
    const { selectedElements } = this.state;
    let value = [];

    if (find(selectedElements, element)) {
      remove(selectedElements, element);
      value = [
        ...selectedElements
      ];
    } else {
      value = [
        ...selectedElements,
        element
      ];
    }

    this.setState({
      selectedElements: value
    });
  }

  render() {
    const { selectedElements, searchTerm, filter} = this.state;
    const { elementList, handleSaveWidgetElements, listOpen, handleCloseList } = this.props;

    return (
      <div className={`elementwidget__list ${listOpen ? 'elementwidget__list--open' : ''}`}>
        <ElementWidgetFilter
          handleSearch={this.handleSearch}
          handleFilter={this.handleFilter}
          handleCloseList={handleCloseList}
          searchTerm={searchTerm}
          filter={filter}
        />
        <div className='elementwidget__options' role='group'>
        {elementList.map(element => {
          if (filter < element.id && element.name.includes(searchTerm)) {
          return (
            <Checkbox
              key={element.guid}
              id={element.guid}
              name={element.guid}
              label={element.name}
              value={JSON.stringify(element)}
              checked={some([...selectedElements], element)}
              disabled={!some([...selectedElements], element) && selectedElements.length >= 3}
              handleChange={this.handleElement}
            />
          )
          }
          else return;
        })}
        </div>
        <div className='elementwidget__selected elementwidget__selected--inside'>
          {(selectedElements.length > 0) ?
            <div className='elementwidget__selectedCounter'>Current selected items:</div>
            : ''
          }
          <ElementWidgetSelected
            selectedElements={selectedElements}
            handleRemoveElement={this.handleRemoveElement}
          />
          <div className='elementwidget__buttons'>
            <Button
              type='button'
              value='Save'
              buttonClassNames='btn btn--secondary elementwidget__buttonSave'
              onClick={() => { handleCloseList(); handleSaveWidgetElements(selectedElements); this.handleResetList(); }}
            />
            <Button
              type='reset'
              value='Cancel'
              buttonClassNames='btn btn--secondary elementwidget__buttonClose'
              onClick={() => { handleCloseList(); this.handleResetList(); }}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default ElementWidgetList;
