import React, { PureComponent, lazy, Suspense, Fragment } from 'react';
import Button from './Form/Element/button';

const SearchFields = lazy(() => import('./SearchFields'));
const AdaptiveList = lazy(() => import('./AdaptiveList'));
const ElementWidget = lazy(() => import('./ElementWidget'));



class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      wrapWide: false
    };
  }

  toggleWrap = wrapWide => {
    this.setState({
      wrapWide: !wrapWide
    });
  }

  render() {
    const { wrapWide } = this.state;

    return (
      <Fragment>
        <Button
          type='button'
          buttonClassNames={`btn btn--wide ${wrapWide ? 'btn--active' : ''}`}
          value='Toggle wide wrap (968px vs 1300px)'
          onClick={() => this.toggleWrap(wrapWide)}
        />
        <Suspense fallback=''>
          <div className={`wrap ${wrapWide ? 'wrap--wide' : ''}`}>
            <header>
              <img className='logo' src={require('../../static/images/logo.png').default} alt='Wargaming Logo' width='196' />
            </header>
            <main className='main'>
              <SearchFields />
              <hr className='separator' />
              <AdaptiveList />
              <hr className='separator' />
              <ElementWidget />
            </main>
          </div>
        </Suspense>
      </Fragment>
    )
  }
}

export default App;
