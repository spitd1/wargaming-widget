import React, { PureComponent, lazy, Fragment } from 'react';

const Button = lazy(() => import('../Form/Element/button'));
const Input = lazy(() => import('../Form/Element/input'));

class SearchFields extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  componentDidMount() {
  }

  render() {
    return (
      <Fragment>
        <section className='section section--primary btn--rounded'>
          <header><h2 className='section__title'>1. Search fields</h2></header>
          <article className='searchfields__item'>
            <form className='searchfields__form'>
              <div className="searchfields__row--half searchfields__hint">
                <h4 className='section__subtitle'><strong>1.1 Solution:</strong> Flexbox and flex value of input is 1.</h4>
              </div>
              <div className='searchfields__row searchfields__row--half searchfields__row--flexbox'>
                <input className='searchfields__input' type='search' placeholder='Type here' />
                <Button
                  value='Search'
                  buttonClassNames='btn btn--primary btn--rounded'
                  // onClick={() => handleMoveToStep('start')}
                />
              </div>
            </form>
          </article>
          <article className='searchfields__item'>
            <form className='searchfields__form'>
              <div className="searchfields__row--half searchfields__hint">
                <h4 className='section__subtitle'><strong>1.2 Solution:</strong> Flexbox and 100% width of input.</h4>
              </div>
              <div className='searchfields__row searchfields__row--half searchfields__row--flexbox2'>
                <input className='searchfields__input' type='search' placeholder='Write something' />
                <Button
                  value='Go'
                  buttonClassNames='btn btn--primary btn--rounded'
                />
              </div>
            </form>
          </article>
          <article className='searchfields__item'>
            <form className='searchfields__form'>
              <div className="searchfields__row--half searchfields__hint">
                <h4 className='section__subtitle'><strong>1.3 Solution:</strong> Grid and min-content.</h4>
              </div>
              <div className='searchfields__row searchfields__row--half searchfields__row--grid'>
                <input className='searchfields__input' type='search' placeholder='Search' />
                <Button
                  value='Try your luck'
                  buttonClassNames='btn btn--primary btn--rounded'
                />
              </div>
            </form>
          </article>
          <article className='searchfields__item'>
            <form className='searchfields__form'>
              <div className="searchfields__row--half searchfields__hint">
                <h4 className='section__subtitle'><strong>1.4 Solution:</strong> Table-cell and nowrap; multiple search inputs.</h4>
              </div>
              <div className='searchfields__row searchfields__row--half searchfields__row--table'>
                <div className='searchfields__cell'>
                  <input className='searchfields__input' type='search' placeholder='Put keywords' />
                </div>
                <div className='searchfields__cell'>
                  <Button
                    value='Find'
                    buttonClassNames='btn btn--primary btn--rounded'
                  />
                </div>
              </div>
              <div className='searchfields__row searchfields__row--half searchfields__row--table'>
                <div className='searchfields__cell'>
                  <input className='searchfields__input' type='search' placeholder='Put search item' />
                </div>
                <div className='searchfields__cell'>
                <Button
                  value='Wish you luck'
                  buttonClassNames='btn btn--primary btn--rounded'
                />
                </div>
              </div>

            </form>
          </article>
        </section>
      </Fragment>
    )
  }
}

export default SearchFields;
