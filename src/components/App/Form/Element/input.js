import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';

class Input extends PureComponent {
  render() {
    const {
      name,
      label,
      value,
      className,
      readonly,
      placeholder,
      type,
      maxLength,
      handleChange
    } = this.props;

    return (
      <div className={`form__row ${className}`}>
        <label htmlFor={name} className='form__label'>{label}</label>
        <input
          className='form__input'
          id={name}
          name={name}
          type={type}
          value={value === null ? '' : value}
          readOnly={readonly}
          onChange={handleChange}
          placeholder={placeholder}
          maxLength={maxLength}
        />
      </div>
    );
  }
}

Input.defaultProps = {
  type: 'text',
  label: '',
  value: '',
  className: '',
  placeholder: null,
  readonly: false
};

Input.propTypes = {
  type: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  readonly: PropTypes.bool,
  placeholder: PropTypes.string,
  maxLength: PropTypes.number.isRequired,
  handleChange: PropTypes.func.isRequired
};

export default Input;
