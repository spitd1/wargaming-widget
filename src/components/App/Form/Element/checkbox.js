import React from 'react';
import { PropTypes } from 'prop-types';

const Checkbox = ({ label, value, checked, disabled, handleChange, name }) => {
  return (
    <div className='form__row'>
      <label
        htmlFor={name}
        className='form__label'
      >
        <input
          type='checkbox'
          className='form__checkbox'
          id={name}
          name={name}
          value={value}
          checked={checked === true ? 'checked' : ''}
          onChange={handleChange}
          disabled={disabled === true ? 'disabled' : ''}
        />
        <span className='label__title'>{label}</span>
      </label>
    </div>
  );
};

Checkbox.defaultProps = {
  label: '',
  name: '',
  checked: false,
  disabled: false
};

Checkbox.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]),
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  handleChange: PropTypes.func.isRequired
};

export default Checkbox;
