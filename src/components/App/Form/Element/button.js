import React from 'react';
import { PropTypes } from 'prop-types';

const Button = ({ type, name, buttonClassNames, value, disabled, sronly, onClick }) => {
  /*
   * JSX block disabled for ESLint type check due to questionable rule implementation
   */

  return (
    /* eslint-disable react/button-has-type */
    <button
      id={name}
      type={type}
      name={name}
      className={buttonClassNames}
      disabled={disabled}
      onClick={onClick}
    ><span className={`${sronly ? 'sr-only' : ''}`}>{value}</span>
    </button>
    /* eslint-enable react/button-has-type */
  );
};

Button.defaultProps = {
  type: 'button',
  name: '',
  buttonClassNames: 'btn btn--rounded btn--primary btn--outline text-light',
  value: 'Odeslat',
  disabled: false,
  sronly: false,
  onClick: () => {}
};

Button.propTypes = {
  type: PropTypes.oneOf(['button', 'submit', 'reset']),
  name: PropTypes.string,
  buttonClassNames: PropTypes.string,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  sronly: PropTypes.bool,
  onClick: PropTypes.func
};

export default Button;
